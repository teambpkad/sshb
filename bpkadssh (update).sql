-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2019 at 07:05 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpkadssh`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `barang_id` int(11) NOT NULL,
  `kode_barang` varchar(60) NOT NULL,
  `nama_barang` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`barang_id`, `kode_barang`, `nama_barang`) VALUES
(2, '1.1.1.0', 'Leptop '),
(4, '1.1.3.0', 'Komputer '),
(6, '1.1.4.0', 'Flasdisk 16GB'),
(7, '1.1.2.0', 'CD RW'),
(9, '1.1.6.0', 'Hardisk'),
(10, '1.1.7.0', 'Monitor'),
(11, '1.1.8.0', 'Keyboard'),
(12, '1.1.9.0', 'Mouse'),
(14, '1.1.11.0', 'Memory PC');

-- --------------------------------------------------------

--
-- Table structure for table `detailsshbrg`
--

CREATE TABLE `detailsshbrg` (
  `detailsshbrg_id` int(11) NOT NULL,
  `grupbrg_id` int(11) NOT NULL,
  `subgrup_id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `kode_sshbrg` varchar(60) NOT NULL,
  `nama_sshbrg` varchar(60) NOT NULL,
  `merk` varchar(60) NOT NULL,
  `spesifikasi` varchar(100) NOT NULL,
  `harga_satuan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grupbrg`
--

CREATE TABLE `grupbrg` (
  `grupbrg_id` int(11) NOT NULL,
  `kode_sshbrggrup` varchar(60) NOT NULL,
  `nama_sshbrggrup` varchar(100) NOT NULL,
  `barang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grupbrg`
--

INSERT INTO `grupbrg` (`grupbrg_id`, `kode_sshbrggrup`, `nama_sshbrggrup`, `barang_id`) VALUES
(3, '8.2.5.1', 'IT', 4),
(4, '8.2.5.2', 'OFFICE', 9);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `satuan_id` int(11) NOT NULL,
  `nama_satuan` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`satuan_id`, `nama_satuan`) VALUES
(1, 'Box'),
(2, 'Pcs'),
(4, 'Botol'),
(6, 'cm'),
(11, '45654'),
(12, 'nmbm'),
(13, 'qweqwe'),
(14, 'asdas');

-- --------------------------------------------------------

--
-- Table structure for table `subgrupbrg`
--

CREATE TABLE `subgrupbrg` (
  `subgrup_id` int(11) NOT NULL,
  `kode_sshsubbrg` varchar(60) NOT NULL,
  `nama_sshsubbrg` varchar(100) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `grupbrg_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subgrupbrg`
--

INSERT INTO `subgrupbrg` (`subgrup_id`, `kode_sshsubbrg`, `nama_sshsubbrg`, `barang_id`, `grupbrg_id`) VALUES
(3, '8.2.5.1.001', 'Hardware', 2, 3),
(4, '8.2.5.2.001', 'Documents', 4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(35) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `level` int(11) NOT NULL COMMENT '1:admin, 2:member'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `nama`, `password`, `email`, `alamat`, `level`) VALUES
(1, 'administrator', 'Ryo Aditya S.Kom', '8cb2237d0679ca88db6464eac60da96345513964', 'ryo.aditya\"gmail.com', 'Taman Raya Tahap 5 Blok LN-8', 1),
(2, 'authliz', 'Jefri Auth Linz', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'jefri@gmail.com', 'Kamboja Bukit Cinta ', 2),
(3, 'Ridwan', 'Ridwan Gajali', '8cb2237d0679ca88db6464eac60da9', '', 'Batu Aji Blok T-8', 1),
(4, 'Rozalah', 'Roza Sarianti', '8cb2237d0679ca88db6464eac60da9', '', 'Botania Blok A-5', 2),
(5, 'Member1', 'Bakri Azura', '8cb2237d0679ca88db6464eac60da9', '', 'Bengkong Abadi Blok C-12', 2),
(6, 'Member2', 'Firman Teru', '8cb2237d0679ca88db6464eac60da9', '', 'Sei Jodoh Laut Blok M-8', 2),
(7, 'Member3', 'Ria Ratnasari', '8cb2237d0679ca88db6464eac60da9', '', 'Tanjung Pantun Blok J-11', 2),
(8, 'Member4', 'Gorosai  Kirai', '8cb2237d0679ca88db6464eac60da9', '', 'Legenda Raya LM-08', 2),
(9, 'Member5', 'Zahara Artique', '8cb2237d0679ca88db6464eac60da9', '', 'Fanindo Karas Blok V-5', 2),
(11, 'Member7', 'Dwi Saputra ', '8cb2237d0679ca88db6464eac60da9', '', 'Batu Besar Kelana Blok P-15', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`barang_id`);

--
-- Indexes for table `detailsshbrg`
--
ALTER TABLE `detailsshbrg`
  ADD PRIMARY KEY (`detailsshbrg_id`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `satuan_id` (`satuan_id`),
  ADD KEY `subgrup_id` (`subgrup_id`),
  ADD KEY `grupbrg_id` (`grupbrg_id`);

--
-- Indexes for table `grupbrg`
--
ALTER TABLE `grupbrg`
  ADD PRIMARY KEY (`grupbrg_id`),
  ADD KEY `barang_id` (`barang_id`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`satuan_id`);

--
-- Indexes for table `subgrupbrg`
--
ALTER TABLE `subgrupbrg`
  ADD PRIMARY KEY (`subgrup_id`),
  ADD KEY `barang_id` (`barang_id`),
  ADD KEY `grupbrg_id` (`grupbrg_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `barang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `detailsshbrg`
--
ALTER TABLE `detailsshbrg`
  MODIFY `detailsshbrg_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grupbrg`
--
ALTER TABLE `grupbrg`
  MODIFY `grupbrg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `satuan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `subgrupbrg`
--
ALTER TABLE `subgrupbrg`
  MODIFY `subgrup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detailsshbrg`
--
ALTER TABLE `detailsshbrg`
  ADD CONSTRAINT `detailsshbrg_ibfk_1` FOREIGN KEY (`satuan_id`) REFERENCES `satuan` (`satuan_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `detailsshbrg_ibfk_2` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`barang_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `detailsshbrg_ibfk_3` FOREIGN KEY (`subgrup_id`) REFERENCES `subgrupbrg` (`subgrup_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `detailsshbrg_ibfk_4` FOREIGN KEY (`grupbrg_id`) REFERENCES `grupbrg` (`grupbrg_id`) ON UPDATE CASCADE;

--
-- Constraints for table `grupbrg`
--
ALTER TABLE `grupbrg`
  ADD CONSTRAINT `grupbrg_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`barang_id`);

--
-- Constraints for table `subgrupbrg`
--
ALTER TABLE `subgrupbrg`
  ADD CONSTRAINT `subgrupbrg_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`barang_id`),
  ADD CONSTRAINT `subgrupbrg_ibfk_2` FOREIGN KEY (`grupbrg_id`) REFERENCES `grupbrg` (`grupbrg_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
