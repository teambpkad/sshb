<?php

class Barang extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		check_not_login();
		$this->load->model('barang_m');
	}

	public function index()
	{
		$data['row'] = $this->barang_m->get();
		$this->template->load('template','barang/barang_data', $data);
	}

	public function add()
	{
		$barang = new stdClass();
		$barang->barang_id = null;
		$barang->kode_barang = null;
		$barang->nama_barang = null;
		$data = array(
			'page' => 'tambah',
			'row'  => $barang
		);
		$this->template->load('template','barang/barang_form', $data);
	}

	public function edit($id)
	{
		$query = $this->barang_m->get($id);
		if($query->num_rows() > 0) {
			$barang = $query->row();
			$data = array(
				'page' => 'edit',
				'row'  => $barang
			);
			$this->template->load('template','barang/barang_form', $data); 
		}else {
			echo "<script>alert('Data tidak ditemukan');";
			echo "window.location='".site_url('barang')."';</script>";
		}
	}

	public function process()
	{
		$post = $this->input->post(null, TRUE);
		if(isset($_POST['tambah'])) {
			$this->barang_m->add($post);
		} else if(isset($_POST['edit'])) {
			$this->barang_m->edit($post);
		}

			if($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success', 'Data berhasil disimpan!');
		}
		redirect('barang'); 
	}

	public function del($id)
	{
		$this->barang_m->del($id);
		if($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success', 'Data berhasil dihapus!');
		}
		redirect('barang');
	}
}