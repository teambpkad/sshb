<?php

class Detailsshbrg extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		check_not_login();
		$this->load->model('detailsshbrg_m');
	}

	public function index()
	{
		$data['row'] = $this->detailsshbrg_m->get();
		$this->template->load('template','Detailsshbrg/detailsshbrg_data', $data);
	}

	// public function add()
	// {
	// 	$Detailsshbrg = new stdClass();
	// 	$Detailsshbrg->barang_id = null;
	// 	$Detailsshbrg->kode_barang = null;
	// 	$Detailsshbrg->nama_barang = null;
	// 	$data = array(
	// 		'page' => 'tambah',
	// 		'row'  => $Detailsshbrg
	// 	);
	// 	$this->template->load('template','Detailsshbrg/detailsshbrg_form', $data);
	// }

	// public function edit($id)
	// {
	// 	$query = $this->detailsshbrg_m->get($id);
	// 	if($query->num_rows() > 0) {
	// 		$Detailsshbrg = $query->row();
	// 		$data = array(
	// 			'page' => 'edit',
	// 			'row'  => $detailsshbrg
	// 		);
	// 		$this->template->load('template','detailsshbrg/detailsshbrg_form', $data); 
	// 	}else {
	// 		echo "<script>alert('Data tidak ditemukan');";
	// 		echo "window.location='".site_url('detailsshbrg')."';</script>";
	// 	}
	// }

	// public function process()
	// {
	// 	$post = $this->input->post(null, TRUE);
	// 	if(isset($_POST['tambah'])) {
	// 		$this->detailsshbrg_m->add($post);
	// 	} else if(isset($_POST['edit'])) {
	// 		$this->detailsshbrg_m->edit($post);
	// 	}

	// 		if($this->db->affected_rows() > 0) {
	// 		$this->session->set_flashdata('success', 'Data berhasil disimpan!');
	// 	}
	// 	redirect('detailsshbrg'); 
	// }

	// public function del($id)
	// {
	// 	$this->detailsshbrg_m->del($id);
	// 	if($this->db->affected_rows() > 0) {
	// 		$this->session->set_flashdata('success', 'Data berhasil dihapus!');
	// 	}
	// 	redirect('detailsshbrg');
	// }
}