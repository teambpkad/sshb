<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>SSHBPKAD</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link rel="icon" href="<?php echo base_url()?>assets/ssh/img/favicon.png">
  <link rel="apple-touch-icon" href="<?php()?>assets/ssh/img/apple-touch-icon.png">

  <!-- Google Fonts -->
  <link  rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700|Roboto:300,400,700&display=swap">

  <!-- Bootstrap CSS File -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/ssh/vendor/bootstrap/css/bootstrap.min.css">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/ssh/vendor/icofont/icofont.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/ssh/vendor/line-awesome/css/line-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/ssh/vendor/aos/aos.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/ssh/vendor/owlcarousel/assets/owl.carousel.min.css">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="<?php echo base_url()?>assets/ssh/css/style.css">
</head>

<body>

  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icofont-close js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>

    <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center">

          <div class="col-6 col-lg-2">
            <h1 class="mb-0 site-logo"><a href="<?php echo base_url('ssh')?>" class="mb-0"><strong>SSH</strong>BPKAD</a></h1>
          </div>

          <div class="col-12 col-md-10 d-none d-lg-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li class="active"><a href="<?php echo base_url('ssh')?>" class="nav-link">Home</a></li>
                <li><a href="<?php echo base_url('contact')?>" class="nav-link">Contact</a></li>
              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-lg-none ml-md-0 py-3" style="position: relative; top: 3px;">
            <a href="#" class="burger site-menu-toggle js-menu-toggle" data-toggle="collapse"
              data-target="#main-navbar">
              <span></span>
            </a>
          </div>

        </div>
      </div>

    </header>


    <main id="main">
      <div class="hero-section border-top border-down">
        <div class="wave">

          <svg width="100%" height="355px" viewBox="0 0 1920 355" version="1.1" xmlns="http://www.w3.org/2000/svg"
            xmlns:xlink="http://www.w3.org/1999/xlink">
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
              <g id="Apple-TV" transform="translate(0.000000, -402.000000)" fill="#FFFFFF">
                <path
                  d="M0,439.134243 C175.04074,464.89273 327.944386,477.771974 458.710937,477.771974 C654.860765,477.771974 870.645295,442.632362 1205.9828,410.192501 C1429.54114,388.565926 1667.54687,411.092417 1920,477.771974 L1920,757 L1017.15166,757 L0,757 L0,439.134243 Z"
                  id="Path"></path>
              </g>
            </g>
          </svg>

        </div>

        <div class="container">
          <div class="row align-items-center">
            <div class="col-12 hero-text-image">
              <div class="row">
                <div class="col-lg-7 text-center text-lg-left">
                  <h1 data-aos="fade-right">STANDAR SATUAN HARGA</h1>
                  <p class="mb-5" data-aos="fade-right" data-aos-delay="100">Sistem Standar Satuan Harga Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam.</p>
                  <p data-aos="fade-right" data-aos-delay="200" data-aos-offset="-500"><a href="<?php echo base_url('auth/login')?>"
                      class="btn btn-outline-white">Login</a></p>
                </div>
                <div class="col-lg-5 iphone-wrap">
                  <img src="<?php echo base_url()?>assets/ssh/img/phone_1.png" alt="Image" class="phone-1" data-aos="fade-right">
                  <img src="<?php echo base_url()?>assets/ssh/img/phone_2.png" alt="Image" class="phone-2" data-aos="fade-right" data-aos-delay="200">
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <div class="site-section">
        <div class="container">

          <div class="row justify-content-center text-center mb-5">
            <div class="col-md-5" data-aos="fade-up">
              <h2 class="section-heading">Sistem Pengelolaan Data Aset</h2>
            </div>
          </div>

          <div class="row">
            <div class="col-md-4" data-aos="fade-up" data-aos-delay="">
              <div class="feature-1 text-center">
                <div class="wrap-icon icon-1">
                  <span class="icon la la-people-carry"></span>
                </div>
                <h3 class="mb-3">Barang</h3>
                <p>Pengelolaan Data Standar Satuan Harga Barang Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam.</p>
              </div>
            </div>
            <div class="col-md-4" data-aos="fade-up" data-aos-delay="100">
              <div class="feature-1 text-center">
                <div class="wrap-icon icon-1">
                  <span class="icon la la-hands-helping"></span>
                </div>
                <h3 class="mb-3">Jasa</h3>
                <p>Pengelolaan Data Standar Satuan Harga Jasa Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam.</p>
              </div>
            </div>
            <div class="col-md-4" data-aos="fade-up" data-aos-delay="200">
              <div class="feature-1 text-center">
                <div class="wrap-icon icon-1">
                  <span class="icon la la-user-tie"></span>
                </div>
                <h3 class="mb-3">Pekerja</h3>
                <p>Pengelolaan Data Standar Satuan Harga Pekerja Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam.</p>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- .site-section -->
    </main>
    <footer class="footer" role="contentinfo">
      <div class="container"> 
        <div class="row justify-content-center text-center">
          <div class="col-md-7">
            <p class="copyright">Copyright &copy;  2019 <strong>SSH</strong>|Badan Pengelolaan Keuangan dan Aset Daerah</p>
            <div class="credits">
              Kota Batam</a>
            </div>
          </div>
        </div>

      </div>
    </footer>
  </div> <!-- .site-wrap -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url()?>assets/ssh/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url()?>assets/ssh/vendor/jquery/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url()?>assets/ssh/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url()?>assets/ssh/vendor/easing/easing.min.js"></script>
  <script src="<?php echo base_url()?>assets/ssh/vendor/php-email-form/validate.js"></script>
  <script src="<?php echo base_url()?>assets/ssh/vendor/sticky/sticky.js"></script>
  <script src="<?php echo base_url()?>assets/ssh/vendor/aos/aos.js"></script>
  <script src="<?php echo base_url()?>assets/ssh/vendor/owlcarousel/owl.carousel.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url()?>assets/ssh/js/main.js"></script>

</body>

</html>
