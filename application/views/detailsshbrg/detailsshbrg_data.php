    <section class="content-header">
      <h1>Barang<small>Pengguna</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url('dashboard') ?>"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Barang</li>
      </ol>
    </section>

    <!-- Main Content -->
    <section class="content">
        <?php $this->view ('massages') ?>
        <div class="box">

        <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><i class="glyphicon glyphicon-plus"></i> Tambah Data SSH Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      
                      <div class="col-md-4 text-info text-center">
                        <?=$this->uri->segment(1) == 'barang' ? 'class="active"' : ''?><a href="<?=site_url('users') ?>"><p class="nav-link small text-info">Grup Barang</p></a>
                        <i class="fas fa-3x glyphicon glyphicon-th-large"></i>
                      </div>

                      <div class="col-md-4 text-info text-center">
                        <a href="<?=site_url('barang') ?>"><p class="nav-link small text-info">Sub Grup Barang </p></a>
                        <i class="fas fa-3x glyphicon glyphicon-th"></i>
                      </div>

                      <div class="col-md-4 text-info text-center">
                         <a href="<?=site_url('satuan') ?>"><p class="nav-link small text-info">Barang</p></a>
                        <i class="fas fa-3x glyphicon glyphicon-th-list"></i>
                      </div>
                    
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>

            <div class="box-header">
                <h3 class="box-title">Standar Satuan Harga Barang</h3>
                    <div class="pull-right">
                    <?php if($this->session->userdata('level') ==1) { ?>
                        <button type="button" class="btn btn-primary btn-flat" data-toggle="modal" data-target="#exampleModal">
                        <i class="glyphicon glyphicon-plus"></i> Tambah Data SSH Barang
                        </button>
                    <?php } ?> 
                    </div>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>Kode SSH</th>
                            <th>Nama SSH</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th>Merk</th>
                            <th>Spesifikasi</th>
                            <th>Satuan</th>
                            <th>Harga</th>
                            <th class="text-center" width="10px">AKSI</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach($row->result() as $key => $data) { ?>
                        <tr>
                            <td width="10px"><?=$no++?>.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><?=$data->merk?></td>
                            <td><?=$data->spesifikasi?></td>
                            <td></td>
                            <td><?=$data->harga_satuan?></td>
                            <td class="text-center" width="100px">
                                <a href="<?=site_url('detailsshbrg/edit/'.$data->detailsshbrg_id)?>" class="btn btn-primary btn-xs"> 
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                                <a href="<?=site_url('detailsshbrg/del/'.$data->detailsshbrg_id)?>" onclick="return confirm('Apakah anda yakin?')" class="btn-danger btn-xs"> <i class="fa fa-trash"></i> Hapus</a>
                            </td> 
                        </tr>
                        <?php
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>     
    </section>