    <section class="content-header">
      <h1>Grup Barang<small>Pengguna</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Grup Barang</li>
      </ol>
    </section>

    <!-- Main Content -->
    <section class="content">
        
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?=ucfirst($page)?> Barang </h3>
                    <div class="pull-right">
                        <a href="<?=site_url('barang ')?>" class="btn btn-warning btn-flat">
                            <i class="fa fa-undo"></i> Kembali
                        </a>
                    </div>
            </div>
            <div class="box-body ">
              <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form action="<?=site_url('barang/process')?>" method="post">
                        <div class="form-group">
                            <label>KODE BARANG</label>
                            <input type="hidden" name="id" value="<?=$row->barang_id?>"> 
                            <input type="text" name="kode_barang" value= "<?=$row->kode_barang?>" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>NAMA BARANG</label>
                            <input type="text" name="nama_barang" value="<?=$row->nama_barang?>" class="form-control" required>
                        </div>       
                        <div class="form-group">
                            <button type="submit" name="<?=$page?>" class="btn btn-success btn-flat"><i class="fa fa-paper-plane"></i> Simpan</button>
                            <button type="Reset" class="btn btn-flat">Riset</button>
                        </div>
                    </form>
                </div>    
              </div>
            </div>
        </div>      
    </section>
