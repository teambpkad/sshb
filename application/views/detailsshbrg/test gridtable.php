<table>
            <tbody><tr>
                <td>
                    <table id="tree-1" border="1" cellpadding="0" cellspacing="0">
                        <tbody>
                        <tr id="tree-head-1">
                            <th>1</th>
                            <th>2</th>
                        </tr>
                        <tr class="treegrid-1 treegrid-expanded" id="node-1">
                            <td>
                                <span class="treegrid-expander treegrid-expander-expanded"></span>1
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-2 treegrid-parent-1 treegrid-expanded" id="node-1-1" style="display: table-row;">
                            <td>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-expander treegrid-expander-expanded"></span>1.1</td><td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-3 treegrid-parent-2" id="node-1-1-1" style="display: table-row;">
                            <td>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-expander"></span>1.1.1
                            </td>
                                <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-9 treegrid-parent-2 treegrid-expanded" id="node-1-1-2" style="display: table-row;">
                            <td>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-expander treegrid-expander-expanded"></span>1.1.2
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-10 treegrid-parent-9" id="node-1-1-2-1" style="display: table-row;">
                            <td>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-indent"></span><span class="treegrid-expander"></span>1.1.2.1
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-4 treegrid-parent-1" id="node-1-2" style="display: table-row;">
                            <td><span class="treegrid-indent"></span>
                                <span class="treegrid-expander"></span>1.2
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-5 treegrid-parent-1 treegrid-expanded" id="node-1-3" style="display: table-row;">
                            <td>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-expander treegrid-expander-expanded"></span>1.3
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-6 treegrid-parent-5" id="node-1-3-1" style="display: table-row;">
                            <td>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-expander"></span>1.3.1
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-7 treegrid-parent-1 treegrid-expanded" id="node-1-4" style="display: table-row;">
                            <td>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-expander treegrid-expander-expanded"></span>1.4
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-8 treegrid-parent-7" id="node-1-4-1" style="display: table-row;">
                            <td><span class="treegrid-indent"></span>
                                <span class="treegrid-indent"></span>
                                <span class="treegrid-expander"></span>1.4.1
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr class="treegrid-12 treegrid-parent-1" id="node-1-5" style="display: table-row;">
                            <td><span class="treegrid-indent"></span>
                                <span class="treegrid-expander"></span>1.5
                            </td>
                            <td>Simple text of ...</td>
                        </tr>

                        <tr class="treegrid-11" id="node-2">
                            <td>
                                <span class="treegrid-expander"></span>2
                            </td>
                            <td>Simple text of ...</td>
                        </tr>
                        <tr id="tree-summary-1">
                            <td>Total</td>
                            <td>343</td>
                        </tr>

                    </tbody>
                </table>
                </td>
            </tr>
        </tbody></table>