    <section class="content-header">
      <h1>Satuan<small>SSHB</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Satuan</li>
      </ol>
    </section>

    <!-- Main Content -->
    <section class="content">
        
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?=ucfirst($page)?> Satuan </h3>
                    <div class="pull-right">
                        <a href="<?=site_url('satuan ')?>" class="btn btn-warning btn-flat">
                            <i class="fa fa-undo"></i> Kembali
                        </a>
                    </div>
            </div>
            <div class="box-body ">
              <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form action="<?=site_url('satuan/process')?>" method="post">
                        <div class="form-group">
                            <label>Nama Satuan</label>
                            <input type="hidden" name="id" value="<?=$row->satuan_id?>"> 
                            <input type="text" name="nama_satuan" value= "<?=$row->nama_satuan?>" class="form-control" required>
                        </div>   
                        <div class="form-group">
                            <button type="submit" name="<?=$page?>" class="btn btn-success btn-flat"><i class="fa fa-paper-plane"></i> Simpan</button>
                            <button type="Reset" class="btn btn-flat">Riset</button>
                        </div>
                    </form>
                </div>    
              </div>
            </div>
        </div>      
    </section>
