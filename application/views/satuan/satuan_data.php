    <section class="content-header">
      <h1>Satuan<small>Pengguna</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url('dashboard') ?>"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">List Satuan</li>
      </ol>
    </section>

    <!-- Main Content -->
    <section class="content">
        <?php $this->view ('massages') ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Satuan</h3>
                    <div class="pull-right">
                        <a href="<?=site_url('satuan/add')?>" class="btn btn-primary btn-flat">
                            <i class="fa fa-user-plus"></i> Tambah
                        </a>
                    </div>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Satuan</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach($row->result() as $key => $data) { ?>
                        <tr>
                            <td width="70px"><?=$no++?>.</td>
                            <td><?=$data->nama_satuan?></td>
                            <td class="text-center" width="160px">
                                <a href="<?=site_url('satuan/edit/'.$data->satuan_id)?>" class="btn btn-primary btn-xs"> 
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                                <a href="<?=site_url('satuan/del/'.$data->satuan_id)?>" onclick="return confirm('Apakah anda yakin?')" class="btn-danger btn-xs"> <i class="fa fa-trash"></i> Hapus</a>
                            </td> 
                            <!-- <td class="text-center" width="160px">
                                <a href="<?=site_url('satuan/edit/'.$data->satuan_id)?>" class="btn btn-primary btn-xs"> 
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                                <a href="<?=site_url('satuan/del/'.$data->satuan_id)?>" onclick="return confirm('Apakah anda yakin?')" class="btn-danger btn-xs"> <i class="fa fa-trash"></i> Hapus</a>
                            </td>  -->
                        </tr>
                        <?php
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>      
    </section>
