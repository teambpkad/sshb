
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SSH | BPKAD BATAM</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/Ionicons/css/ionicons.min.css">

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->

  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=base_url('dashboard') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>SH</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SSH</b> BPKAD</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url() ?>assets/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->fungsi->user_login()->level == 1 ? "Administrator" : "Member"?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url() ?>assets/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  <?php echo ucfirst($this->fungsi->user_login()->nama)?>
                  <small><?php echo ($this->fungsi->user_login()->alamat)?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?=site_url('auth/logout')  ?>" class="btn btn-default btn-flat">Logout</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
   <!--    <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>assets/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo ucfirst($this->fungsi->user_login()->username)?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div> -->
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li <?=$this->uri->segment(1) == 'dashboard' || $this->uri->segment(1) == '' ? 'class="active"' : ''?>>
          <a href="<?=base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview <?=$this->uri->segment(1) == 'detailsshbrg' || $this->uri->segment(1) == '#' || $this->uri->segment(1) == '#' ? 'active' : ''?>">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Standar Satuan Harga</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?=$this->uri->segment(1) == 'detailsshbrg' ? 'class= "active"' : ''?>><a href="<?=base_url('detailsshbrg') ?>"><i class="fa fa-circle-o"></i>Barang</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-danger"></i>Jasa</a></li>
            <li><a href="#"><i class="fa fa-circle-o text-danger"></i>Pekerja</a></li>
          </ul>
        </li>
        <li class="treeview <?=$this->uri->segment(1) == 'rekapsshbrg' || $this->uri->segment(1) == 'rekapsshjas' || $this->uri->segment(1) == 'rekapsshpek' ? 'active' : ''?>">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Rekap Data SSH</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="<?=site_url('#') ?>"><i class="fa fa-circle-o"></i>SSH Barang</a></li>
            <li><a href="<?=site_url('#') ?>"><i class="fa fa-circle-o text-danger"></i>SSH Jasa</a></li>
            <li><a href="<?=site_url('#') ?>"><i class="fa fa-circle-o text-danger"></i>SSH Pekerja</a></li>
          </ul>
        </li>
        <li class="treeview <?=$this->uri->segment(1) == '#' || $this->uri->segment(1) == '#' || $this->uri->segment(1) == '#' ? 'active' : ''?>">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Informasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=site_url('#') ?>"><i class="fa fa-circle-o"></i>Usulan</a></li>
            <li><a href="<?=site_url('#') ?>"><i class="fa fa-circle-o"></i>Pembahasan Usulan</a></li>
            <li><a href="<?=site_url('#') ?>"><i class="fa fa-circle-o"></i>Pengumuman</a></li>
          </ul>
        </li>

        <?php if($this->session->userdata('level') ==1) { ?>

        <li class="header">CONTROL PANEL</li>
        <li class="treeview <?=$this->uri->segment(1) == 'users' || $this->uri->segment(1) == 'barang' || $this->uri->segment(1) == 'satuan' ? 'active' : ''?>">
          <a href="#">
            <i class="fa fa-laptop"></i> <span>Administrator</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li <?=$this->uri->segment(1) == 'users' ? 'class= "active"' : ''?>>
              <a href="<?=site_url('users')  ?>"><i class="fa fa-circle-o"></i>Users</a></li>
           <!--  <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i>Standart Satuan Harga
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i>Barang</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Jasa</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Pekerja</a></li>
              </ul>
            </li> -->
            <li <?=$this->uri->segment(1) == 'barang' ? 'class="active"' : ''?>><a href="<?=site_url('barang') ?>"><i class="fa fa-circle-o"></i>List Barang</a></li>
            <li <?=$this->uri->segment(1) == 'satuan' ? 'class="active"' : ''?>><a href="<?=site_url('satuan') ?>"><i class="fa fa-circle-o"></i>List Satuan</a></li>    
          </ul>
        </li>
      <?php } ?>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <?php echo $contents ?>
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <strong>Copyright &copy; 2019-2025 <a href="http://localhost:8080/sshb/dashboard">SSH-BPKAD BATAM</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Datatables -->
<script src="<?php echo base_url() ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/js/adminlte.min.js"></script>
<script>
  $(document).ready(function(){
   // Initialize
   $('#table1').DataTable({
    "ordering": false,
    "aLengthMenu": [[5, 10],[ 5, 10]]
   });
});
</script>

</body>
</html>
