    <section class="content-header">
      <h1>User<small>Pengguna</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url('dashboard') ?>"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Users</li>
      </ol>
    </section>

    <!-- Main Content -->
    <section class="content">
    	
    	<div class="box">
    		<div class="box-header">
    			<h3 class="box-title">User Portal Sistem</h3>
    				<div class="pull-right">
    					<a href="<?=site_url('users/add')?>" class="btn btn-primary btn-flat">
    						<i class="fa fa-user-plus"></i>Tambah
    					</a>
    				</div>
    		</div>
			<div class="box-body table-responsive">
    			<table class="table table-bordered table-striped" id="table1">
    				<thead>
    					<tr>
    						<th>No</th>
    						<th>Username</th>
    						<th>Nama</th>
    						<th>Alamat</th>
    						<th>Level</th>
    						<th class="text-center">Aksi</th>
    					</tr>
    				</thead>
	    			<tbody>
	    				<?php $no = 1;
	    				foreach($row->result() as $key => $data) { ?>
	    				<tr>
	    					<td><?=$no++?>.</td>
	    					<td><?=$data->username?></td>
	    					<td><?=$data->nama?></td>
	    					<td><?=$data->alamat?></td>
	    					<td><?=$data->level == 1 ? "Admin" : " Member"?></td>
	    					<td class="text-center" width="150px">
	    						<form action="<?=site_url('users/del')?>" method="post">
                                <a href="<?=site_url('users/edit/'.$data->user_id)?>" class="btn btn-primary btn-xs"> 
	    							<i class="fa fa-pencil"></i> Edit
	    						</a>
                                <input type="hidden" name="user_id" value="<?=$data->user_id?>">
                                    <button onclick="return confirm('Apakah anda yakin?')" class="btn btn-danger btn-xs">
    	    							<i class="fa fa-trash"></i> Hapus
                                    </button>
                                </form>
	    					</td> 
	    				</tr>
	    				<?php
	    				} ?>
	    			</tbody>
	    		</table>
		    </div>
		</div>	    
	</section>
