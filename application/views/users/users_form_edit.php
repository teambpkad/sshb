    <section class="content-header">
      <h1>Users<small>Pengguna</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Users</li>
      </ol>
    </section>

    <!-- Main Content -->
    <section class="content">
    	
    	<div class="box">
    		<div class="box-header">
    			<h3 class="box-title">Edit Users</h3>
    				<div class="pull-right">
    					<a href="<?=site_url('users')?>" class="btn btn-warning btn-flat">
    						<i class="fa fa-undo"></i> Kembali
    					</a>
    				</div>
    		</div>
			<div class="box-body ">
		      <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form action="" method="post">
                        <div class="form-group <?=form_error('fullname') ? 'has-error' : null?>">
                            <label>Nama</label>
                            <input type="hidden" name="user_id" value="<?=$row->user_id?>">
                            <input type="text" name="fullname" value="<?=$this->input->post('fullname') ?? $row->nama?>" class="form-control">
                            <?=form_error('fullname')?>
                        </div>
                        <div class="form-group <?=form_error('username') ? 'has-error' : null?>">
                            <label>Username</label>
                            <input type="text" name="username" value="<?=set_value('username')?>" class="form-control">
                            <?=form_error('username')?>                            
                        </div>
                        <div class="form-group <?=form_error('password') ? 'has-error' : null?>">
                            <label>Password</label> <small>(Biarkan kosong jika tidak ingin diganti)</small>
                            <input type="password" name="password" value="<?=set_value('password')?>" class="form-control">
                            <?=form_error('password')?>  
                        </div>
                        <div class="form-group <?=form_error('passkonf') ? 'has-error' : null?>">
                            <label>Password Konfirmasi</label>
                            <input type="password" name="passkonf" value="<?=set_value('passkonf')?>" class="form-control">
                            <?=form_error('passkonf')?>
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" class="form-control"><?=set_value('alamat')?></textarea>
                            <?=form_error('alamat')?>
                        </div>
                        <div class="form-group <?=form_error('level') ? 'has-error' : null?>">
                            <label>Level</label>
                            <select name="level" class="form-control">
                                <?php $level = $this->input->post('level') ? $this->input->post('level') : $row->level ?>
                                <option value="1">Admin</option>
                                <option value="2" <?=$level == 2 ? 'selected' : null?>>Member</option>
                            </select>
                            <?=form_error('level')?>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-paper-plane"></i> Simpan</button>
                            <button type="Reset" class="btn btn-flat">Riset</button>
                        </div>
                    </form>
                </div>    
              </div>
		    </div>
		</div>	    
	</section>
