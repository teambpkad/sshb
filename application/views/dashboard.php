    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="alert alert-success" role="alert">
          <h4 class="alert-heading">Selamat Datang!</h4>
          <p>Selamat Datang<strong> <?php echo ucfirst($this->fungsi->user_login()->nama)?> </strong> di Sistem Standar Satuan Harga Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam, Anda Login Sebagai <strong><?php echo ($this->fungsi->user_login()->level == 1 ? "Admin" : " Member")?></strong>.</p>
          <?php if($this->session->userdata('level') ==1) { ?>
          <hr>
                 <!-- Button trigger modal -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            <i class="fas
glyphicon glyphicon-cog"></i> Control Panel
          </button>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fas
glyphicon glyphicon-cog"></i> Control Panel Administrator</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
              </div>
              <div class="modal-body">
                <div class="row">
                  
                  <div class="col-md-4 text-info text-center">
                    <?=$this->uri->segment(1) == 'barang' ? 'class="active"' : ''?><a href="<?=site_url('users') ?>"><p class="nav-link small text-info">USERS</p></a>
                    <i class="fas fa-3x glyphicon glyphicon-user"></i>
                  </div>

                  <div class="col-md-4 text-info text-center">
                    <a href="<?=site_url('barang') ?>"><p class="nav-link small text-info">LIST BARANG</p></a>
                    <i class="fas fa-3x glyphicon glyphicon-tasks"></i>
                  </div>

                  <div class="col-md-4 text-info text-center">
                     <a href="<?=site_url('satuan') ?>"><p class="nav-link small text-info">LIST SATUAN</p></a>
                    <i class="fas fa-3x glyphicon glyphicon-list-alt"></i>
                  </div>

             <!--      <div class="col-md-3 text-info text-center">
                     <a href="<?=site_url('detailsshbrg') ?>"><p class="nav-link small text-info">SSH BARANG</p></a>
                    <i class="fas fa-3x glyphicon glyphicon-shopping-cart"></i>
                  </div> -->
                
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-th-large"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">grup BARANG</span>
              <span class="info-box-number">60</span>
            </div>
          </div>
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="glyphicon glyphicon-th"></i></i></span>
            <div class="info-box-content">
              <span class="info-box-text">SUB BARANG</span>
              <span class="info-box-number">30</span>
            </div>
          </div>
        </div>

        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="glyphicon glyphicon-th-list"></i></i></span>
            <div class="info-box-content">
              <span class="info-box-text">DETAIL SSH BARANG</span>
              <span class="info-box-number">30</span>
            </div>
          </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-dark"><i class="glyphicon glyphicon-user"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">ANGGOTA</span>
              <span class="info-box-number">27</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-dark"><i class="ion ion-ios-chatboxes"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">USULAN MEMBER</span>
              <span class="info-box-number">6</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-dark"><i class="glyphicon glyphicon-duplicate"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">PENGUMUMAN</span>
              <span class="info-box-number">6</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </div>
      <?php } ?>
    </section>
    <!-- /.content -->