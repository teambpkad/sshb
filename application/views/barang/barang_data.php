    <section class="content-header">
      <h1>Barang<small>Pengguna</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?=base_url('dashboard') ?>"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">List Barang</li>
      </ol>
    </section>

    <!-- Main Content -->
    <section class="content">
        <?php $this->view ('massages') ?>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Daftar Barang</h3>
                    <div class="pull-right">
                        <a href="<?=site_url('barang/add')?>" class="btn btn-primary btn-flat">
                            <i class="fa fa-user-plus"></i> Tambah
                        </a>
                    </div>
            </div>
            <div class="box-body table-responsive">
                <table class="table table-bordered table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode Barang</th>
                            <th>Nama Barang</th>
                            <th class="text-center">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach($row->result() as $key => $data) { ?>
                        <tr>
                            <td width="70px"><?=$no++?>.</td>
                            <td><?=$data->kode_barang?></td>
                            <td><?=$data->nama_barang?></td>
                            <td class="text-center" width="160px">
                                <a href="<?=site_url('barang/edit/'.$data->barang_id)?>" class="btn btn-primary btn-xs"> 
                                    <i class="fa fa-pencil"></i> Edit
                                </a>
                                <a href="<?=site_url('barang/del/'.$data->barang_id)?>" onclick="return confirm('Apakah anda yakin?')" class="btn-danger btn-xs"> <i class="fa fa-trash"></i> Hapus</a>
                            </td> 
                        </tr>
                        <?php
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>      
    </section>
