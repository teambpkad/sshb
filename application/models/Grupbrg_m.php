<?php

class Grupbrg_m extends CI_Model{

	public function get($id= null)
	{
		$this->db->from('grupbrg');
		if($id != null) {
			$this->db->where('grupbrg_id', $id);
		}
		$query = $this->db->get();
		return $query;
	}

	public function add($post)
	{
		$params = [
			'kode_barang' => $post['kode_barang'],
			'nama_barang' => $post['nama_barang'],
			'kode_sshbrggrup' => $post['kode_sshbrggrup'],
			'nama_sshbrggrup' => $post['nama_sshbrggrup'],
		];
		$this->db->insert('grupbrg', $params);
	}

	public function edit($post)
	{
		$params = [
			'kode_barang' => $post['kode_barang'],
			'nama_barang' => $post['nama_barang'],
		];
		$this->db->where('grupbrg_id', $post['id']);
		$this->db->update('grupbrg', $params);
	}
	
	public function del($id)
	{
		$this->db->where('grupbrg_id', $id);
		$this->db->delete('grupbrg');
	}
}