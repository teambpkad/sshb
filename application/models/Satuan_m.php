<?php

class Satuan_m extends CI_Model{

	public function get($id= null)
	{
		$this->db->from('satuan');
		if($id != null) {
			$this->db->where('satuan_id', $id);
		}
		$query = $this->db->get();
		return $query;
	}

	public function add($post)
	{
		$params = [
			'nama_satuan' => $post['nama_satuan']
		];
		$this->db->insert('satuan', $params);
	}

	public function edit($post)
	{
		$params = [
			'nama_satuan' => $post['nama_satuan']
		];
		$this->db->where('satuan_id', $post['id']);
		$this->db->update('satuan', $params);
	}
	
	public function del($id)
	{
		$this->db->where('satuan_id', $id);
		$this->db->delete('satuan');
	}
}