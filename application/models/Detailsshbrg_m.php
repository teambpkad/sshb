<?php

class Detailsshbrg_m extends CI_Model{

	public function get($id= null)
	{
		$this->db->select('*');
		$this->db->from('detailsshbrg');
		if($id != null) {

	    $this->db->join('grupbrg','grupbrg_id=detailsshbrg_id');
 		$this->db->join('subgrupbrg','tbstatus.idpendidikan=tbsekolah.idpendidikan');
 		$this->db->join('satuan','tbsekolah.id=tbrakyat.id');
   		$this->db->join('barang','tbstatus.idpendidikan=tbsekolah.idpendidikan');
		$this->db->where('detailsshbrg_id', $id);
		}
		$query = $this->db->get();
		return $query;
	}

	// public function add($post)
	// {
	// 	$params = [
	// 		'kode_detailsshbrg' => $post['kode_barang'],
	// 		'nama_barang' => $post['nama_barang'],
	// 	];
	// 	$this->db->insert('barang', $params);
	// }

	// public function edit($post)
	// {
	// 	$params = [
	// 		'kode_barang' => $post['kode_barang'],
	// 		'nama_barang' => $post['nama_barang'],
	// 	];
	// 	$this->db->where('barang_id', $post['id']);
	// 	$this->db->update('barang', $params);
	// }
	
	// public function del($id)
	// {
	// 	$this->db->where('barang_id', $id);
	// 	$this->db->delete('barang');
	// }
}